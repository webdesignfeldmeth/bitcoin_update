Vue.component('dashboard-currency-item', {
	props: [
		"headline",
		"value",
		"symbol"
	],
	template: `
		<div class="col-lg-4 col-md-4 col-sm-12 item">
			<h4>{{headline}}</h4>
			<span>{{value}} {{symbol}}</span>
		</div>
	`
});